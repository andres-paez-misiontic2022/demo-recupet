import 'package:apprecupet/read%20data/get_user_name.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Mysquare extends StatelessWidget {
  final String child;

  // crear una lista de numeros de documentos
  List<String> docIDs = [];

  //trae los documentos
  Future getDocId() async {
    await FirebaseFirestore.instance.collection('users').get().then(
          (snapshot) => snapshot.docs.forEach(
            (document) {
              print(document.reference);
              docIDs.add(document.reference.id);
            },
          ),
        );
  }

  Mysquare({required this.child});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 500,
        color: Color.fromARGB(255, 191, 246, 204),
        //child: Text(child),
        child: FutureBuilder(
          future: getDocId(),
          builder: (context, snapshot) {
            return ListView.builder(
              itemCount: docIDs.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    title: GetUserName(documentId: docIDs[index]),
                    tileColor: Color.fromARGB(255, 191, 246, 204),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
