// ignore_for_file: prefer_const_constructors
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  final VoidCallback showLoginPage;
  const FormPage({
    Key? key,
    required this.showLoginPage,
  }) : super(key: key);

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final user = FirebaseAuth.instance.currentUser!;

//text controllers
  final _tiporeporteController = TextEditingController();
  final _celularpersonalController = TextEditingController();
  final _celularsecundarioController = TextEditingController();
  final _ubicacionController = TextEditingController();
  final _fechaController = TextEditingController();
  final _horaController = TextEditingController();
  final _tipomascotaController = TextEditingController();
  final _razaController = TextEditingController();
  final _colorController = TextEditingController();
  final _generoController = TextEditingController();
  final _edadmascotaController = TextEditingController();
  final _datosadicionalesController = TextEditingController();

  @override
  void dispose() {
    _tiporeporteController.dispose();
    _celularpersonalController.dispose();
    _celularsecundarioController.dispose();
    _ubicacionController.dispose();
    _fechaController.dispose();
    _horaController.dispose();
    _tipomascotaController.dispose();
    _razaController.dispose();
    _colorController.dispose();
    _generoController.dispose();
    _edadmascotaController.dispose();
    _datosadicionalesController.dispose();
    super.dispose();
  }

  Future<dynamic> SubmitButtonInputElement() async {
    await FirebaseAuth.instance.userChanges();
    // adiciona detalles del usuario
    addUserDetails(
      _tiporeporteController.text.trim(),
      _celularpersonalController.text.trim(),
      _celularsecundarioController.text.trim(),
      _ubicacionController.text.trim(),
      _fechaController.text.trim(),
      _horaController.text.trim(),
      _tipomascotaController.text.trim(),
      _razaController.text.trim(),
      _colorController.text.trim(),
      _generoController.text.trim(),
      int.parse(_edadmascotaController.text.trim()),
      _datosadicionalesController.text.trim(),
    );
  }

  Future addUserDetails(
      String tiporeporte,
      String celularpersonal,
      String celularsecundario,
      String ubicacion,
      String fecha,
      String hora,
      String tipomascota,
      String raza,
      String color,
      String genero,
      int edadmascota,
      String datosadicionales) async {
    await FirebaseFirestore.instance.collection('reportes').add({
      'tipo reporte': tiporeporte,
      'celular personal': celularpersonal,
      'celular secundario': celularsecundario,
      'ubicacion': ubicacion,
      'fecha': fecha,
      'hora': hora,
      'tipo mascota': tipomascota,
      'raza': raza,
      'color': color,
      'genero': genero,
      'edad mascota': edadmascota,
      'datosadicionales': datosadicionales,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 149, 226, 188),
        elevation: 4,
        title: Text(
          user.email!,
          style: TextStyle(fontSize: 16),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              FirebaseAuth.instance.signOut();
            },
            child: Icon(Icons.logout),
          ),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 149, 226, 188),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                /*Icon(
                  Icons.pets,
                  color: Color.fromARGB(255, 20, 95, 23),
                  size: 50,
                ),*/
                Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Image.asset(
                    "lib/imagenes/Recupet logo fondo blanco.png",
                    scale: 3,
                  ),
                ),
                SizedBox(height: 5),

                //saludo superior
                Text(
                  "Recupet",
                  style: TextStyle(
                    color: Color.fromARGB(255, 20, 95, 23),
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "DIligencia los datos para el reporte",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 30),

                // _tiporeporteController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _tiporeporteController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Tipo reporte Perdido o Encontrado',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _celularpersonalController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _celularpersonalController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa tu celular personal',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _celularsecundarioController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _celularsecundarioController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa tu celular secundario',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _ubicacionController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _ubicacionController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa tu ubicacion',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _fechaController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _fechaController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa la fecha del reporte',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _horaController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _horaController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa la hora del reporte',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _tipomascotaController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _tipomascotaController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa el tipo de mascota',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _razaController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _razaController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa la raza de la mascota',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _colorController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _colorController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa el color de la mascota',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _generoController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _generoController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa el género de la mascota',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _edadmascotaController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _edadmascotaController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'Ingresa edad de la mascota',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // _datosadicionalesController textfield
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _datosadicionalesController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      hintText: 'registra datos adicionales',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                  ),
                ),
                SizedBox(height: 10),

                // boton de submit

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: SubmitButtonInputElement,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 20, 95, 23),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                        child: Text(
                          "Reportar",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
