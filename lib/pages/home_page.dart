import 'package:apprecupet/square.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final user = FirebaseAuth.instance.currentUser!;

// crear una lista de numeros de documentos
  List<String> docIDs = [];

  //trae los documentos
  Future getDocId() async {
    await FirebaseFirestore.instance
        .collection('users')
        .orderBy('age', descending: true)
        .get()
        .then(
          (snapshot) => snapshot.docs.forEach(
            (document) {
              print(document.reference);
              //docIDs.add(document.reference.id);
            },
          ),
        );
  }

  /*@override
  void initState() {
    getDocId();
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 149, 226, 188),
        elevation: 4,
        title: Text(
          //'Últimas Noticias ' + user.email!,
          'Últimos Reportes',
          style: TextStyle(fontSize: 16),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              FirebaseAuth.instance.signOut();
            },
            child: Icon(Icons.logout),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            /*Expanded(
              child: FutureBuilder(
                future: getDocId(),
                builder: (context, snapshot) {
                  return ListView.builder(
                    itemCount: docIDs.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          title: GetUserName(documentId: docIDs[index]),
                          tileColor: Color.fromARGB(255, 191, 246, 204),
                        ),
                      );
                    },
                  );
                },
              ),
            ),*/
            Expanded(
              child: ListView.builder(
                  itemCount: docIDs.length,
                  itemBuilder: (context, index) {
                    return Mysquare(
                      child: docIDs[index],
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
